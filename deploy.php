<?php
# Automated Deployments from Bitbucket Service Hook
# Borrowed from: http://brandonsummers.name/blog/2012/02/10/using-bitbucket-for-automated-deployments/ and tweaked
# Prerequisites:
# * repository should be cloned using SSH URL (git clone --mirror git@bitbucket.org:<username>/<repo-name>.git)
# * SSH keys for user under which account PHP runs should be created and configured
# * POST hook on Bitbucket pointing to the script URL should be created
#
# See detals here: http://brandonsummers.name/blog/2012/02/10/using-bitbucket-for-automated-deployments/,
# http://symmetrycode.com/super-easy-deployment-with-git-and-bitbucket/,
# https://confluence.atlassian.com/display/BITBUCKET/Use+deployment+keys

date_default_timezone_set('Europe/Moscow');

class Deploy {

    /**
     * A callback function to call after the deploy has finished.
     *
     * @var callback
     */
    public $post_deploy;

    /**
     * The name of the file that will be used for logging deployments. Set to
     * FALSE to disable logging.
     *
     * @var string
     */
    private $_log = 'deployments.log';
    private $_logdir;

    /**
     * The timestamp format used for logging.
     *
     * @link    http://www.php.net/manual/en/function.date.php
     * @var     string
     */
    private $_date_format = 'Y-m-d H:i:sP';

    /**
     * The name of the branch to pull from.
     *
     * @var string
     */
    private $_branch = 'develop';

    /**
     * The name of the remote to pull from.
     *
     * @var string
     */
    private $_remote = 'origin';

    /**
     * The directory where your website and git repository are located, can be
     * a relative or absolute path
     *
     * @var string
     */
    private $_directory;

    /**
     * Sets up defaults.
     *
     * @param  string  $directory  Directory where your website is located
     * @param  array   $data       Information about the deployment
     */
    public function __construct($directory, $options = array())
    {
        // Determine the directory path
        $this->_directory = realpath($directory).DIRECTORY_SEPARATOR;

        $available_options = array('log', 'date_format', 'branch', 'remote');

        foreach ($options as $option => $value)
        {
            if (in_array($option, $available_options))
            {
                $this->{'_'.$option} = $value;
            }
        }

        $this->log('Attempting deployment...');
    }

    /**
     * Writes a message to the log file.
     *
     * @param  string  $message  The message to write
     * @param  string  $type     The type of log message (e.g. INFO, DEBUG, ERROR, etc.)
     */
    public function log($message, $type = 'INFO')
    {
        if ($this->_log)
        {
            // Set the name of the log file
            $filename = $this->_logdir.'/'.$this->_log;
            echo $filename;

            if ( ! file_exists($filename))
            {
                // Create the log file
                file_put_contents($filename, '');

                // Allow anyone to write to log files
                chmod($filename, 0666);
            }

            // Write the message into the log file
            // Format: time --- type: message
            file_put_contents($filename, date($this->_date_format).' --- '.$type.': '.$message.PHP_EOL, FILE_APPEND);
        }
    }

    /**
     * Executes the necessary commands to deploy the website.
     */
    public function execute()
    {
        try
        {
            // Debug ssh connect to bitbucket.org
            //echo(shell_exec('ssh -T -v git@bitbucket.org 2>&1'));

            // Make sure we're in the right directory
            $dir = getcwd();
            $this->_logdir = $dir;
            $this->log('Current working directory... '.$dir);
            chdir($this->_directory);
            $dir = getcwd();
            $this->log('Changing working directory... '.$dir);

            // Discard any changes to tracked files since our last deploy
            $result = shell_exec('git reset --hard HEAD 2>&1');
            $this->log('Reseting repository... '.$result);

            // Update the local repository
            $result = shell_exec('git pull '.$this->_remote.' '.$this->_branch.' 2>&1');
            $this->log('Pulling in changes... '.$result);

            // Secure the .git directory
            $result = shell_exec('chmod -R og-rx .git 2>&1');
            log('Securing .git directory... '.$result);

            if (is_callable($this->post_deploy))
            {
                call_user_func($this->post_deploy, $this->_data);
            }

            $this->log('Deployment successful.');
        }
        catch (Exception $e)
        {
            $this->log($e, 'ERROR');
        }
    }

}


echo("running...");

$sitedir = '/var/www/gittest/'; //put path to site directory with git repository here

$deploy = new Deploy($sitedir);

// This is just an example
//$deploy->post_deploy = function() use ($deploy) {
    // hit the wp-admin page to update any db changes
//    exec('curl http://www.foobar.com/wp-admin/upgrade.php?step=upgrade_db');
//    $deploy->log('Doing nothing... ');
//};

$deploy->execute();

echo("finished...");

?>
